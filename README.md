## Getting started

At the moment this package depends on an unpublished package
called Cuddle. If you have access to this repo you should also
have access to the repo for Cuddle so you can do a local install
if you want.

You can start training immediately:
```bash
python main.py
```
The training data is generated randomly and on the fly, so there's no need to 
create any data beforehand.

The model is saved after every X steps. At any time during training you can 
interrupt the program with Ctrl + C. The resulting KeyboardInterrupt
will be caught, and before exiting the program the results and loss on
the validation set will be printed. In addition, the attention mechanism is 
visualized for each instance in the validation set.

Note that it takes a few epochs before you'll reach decent results, and
for good generalization you might also want to generate more than 1000 training
samples.
