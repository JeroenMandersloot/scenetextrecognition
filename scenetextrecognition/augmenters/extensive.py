import imgaug.augmenters as iaa

from scenetextrecognition.augmenters.augmenter import Augmenter


class ExtensiveAugmenter(Augmenter):
    def __init__(self):
        self._augmenter = iaa.Sequential([
            iaa.Invert(0.5),
            iaa.GaussianBlur((0, 0.5)),
            iaa.Multiply((0.5, 1.5)),
            iaa.Sometimes(0.5, iaa.MotionBlur((3, 5))),
            iaa.Sometimes(0.5, iaa.JpegCompression(compression=(50, 80))),
            iaa.Sometimes(0.5, iaa.Sharpen(alpha=(0.0, 1.0), lightness=(0.75, 1.)))
        ])

    def augment(self, image):
        image = self._augmenter.augment_image(image)
        return image
