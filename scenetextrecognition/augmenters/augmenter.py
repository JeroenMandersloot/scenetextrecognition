import abc


class Augmenter:
    @abc.abstractmethod
    def augment(self, image):
        raise NotImplementedError
