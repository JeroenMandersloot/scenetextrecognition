import os
import random
from typing import Generator

import tensorflow as tf


def walk_dir(root: str, shuffle: bool = False) -> Generator[str, None, None]:
    """
    Recursively walk a `root` directory and return the full paths to all files
    contained within. If `shuffle` is set to True, directories will be
    traversed in a random order.

    :param root: str, the root directory to traverse.
    :param shuffle: bool, whether or not to traverse subdirectories randomly.
    :return: Generator[str, None, None]
    """
    paths = [os.path.join(root, c) for c in os.listdir(root)]
    if shuffle:
        random.shuffle(paths)
    for path in filter(os.path.isdir, paths):
        for file in walk_dir(path, shuffle):
            yield file
    files = filter(os.path.isfile, paths)
    for file in files:
        yield file


def fix_tensorflow_rtx():
    """
    A fix for RTX 2070 card, without this the code doesn't run.
    """
    gpu_devices = tf.config.experimental.list_physical_devices('GPU')
    for device in gpu_devices:
        tf.config.experimental.set_memory_growth(device, True)
