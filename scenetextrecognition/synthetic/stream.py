import abc
import base64
import os
import random
from typing import Tuple, Any, Iterator

from PIL import Image

from scenetextrecognition.synthetic.data_generator import DataGenerator


class SyntheticDataStream:
    @abc.abstractmethod
    def __len__(self) -> int:
        raise NotImplementedError

    @abc.abstractmethod
    def __iter__(self) -> Iterator[Tuple[Image.Image, str]]:
        raise NotImplementedError


class SyntheticDataStreamFromGenerator(SyntheticDataStream):
    def __init__(self,
                 generator: DataGenerator,
                 num_instances: int):
        self.generator = generator
        self.num_instances = num_instances

    def __len__(self) -> int:
        return self.num_instances

    def __iter__(self) -> Iterator[Tuple[Image.Image, str]]:
        for _ in range(self.num_instances):
            yield self.generator.generate_instance()


class SyntheticDataStreamFromDisk(SyntheticDataStream):
    def __init__(self, root: str):
        self.root = root
        self.paths = [os.path.join(root, f) for f in os.listdir(root)]

    @staticmethod
    def get_text_from_filename(filename: str) -> str:
        """
        Extracts the text on an image from its filename.

        :param filename: str
        :return: str
        """
        return base64.b64decode(
            os.path.splitext(filename)[0].split('_')[1], b'+-'
        ).decode('utf-8')

    def __len__(self) -> int:
        return len(self.paths)

    def __iter__(self) -> Iterator[Any]:
        paths = random.sample(self.paths, len(self.paths))
        for path in paths:
            image = Image.open(path)
            text = self.get_text_from_filename(os.path.basename(path))
            yield image, text
