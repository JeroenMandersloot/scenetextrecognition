import base64
import os
import random
import uuid
from enum import auto, Enum
from typing import Optional, Tuple

import numpy as np
from PIL import Image, ImageFont, ImageDraw
from PIL.Image import BILINEAR
from cuddle.data.vocabulary import Vocabulary

from scenetextrecognition.util import walk_dir


class BackgroundType(Enum):
    COLOR = auto()
    NOISE = auto()
    IMAGE = auto()


class DataGenerator:
    # Class variables below are controlled by `color_variation`
    MAX_BACKGROUND_DARKNESS = 50
    MAX_TEXT_COLOR_VALUE = 40
    MAX_NOISE_STD = 40

    def __init__(self,
                 fonts_dir: str,
                 vocabulary: Vocabulary,
                 min_num_tokens: int,
                 max_num_tokens: int,
                 width: Optional[int] = None,
                 height: Optional[int] = None,
                 delimiter: str = '',
                 min_letter_spacing: int = 0,
                 max_letter_spacing: int = 5,
                 min_padding_x: int = 0,
                 max_padding_x: int = 50,
                 min_padding_y: int = 0,
                 max_padding_y: int = 10,
                 min_font_size: int = 8,
                 max_font_size: int = 64,
                 text_angle_std: int = 2,
                 color_variation: float = 1,
                 grayscale: bool = False,
                 background_dir: Optional[str] = None,
                 background_overlay: float = 0.5,
                 output_dir: Optional[str] = None):

        assert 0 <= color_variation <= 1
        assert 0 <= background_overlay <= 1
        self.vocabulary = vocabulary
        self.min_num_tokens = min_num_tokens
        self.max_num_tokens = max_num_tokens
        self.height = height
        self.width = width
        self.min_letter_spacing = min_letter_spacing
        self.max_letter_spacing = max_letter_spacing
        self.color_variation = color_variation
        self.min_padding_x = min_padding_x
        self.max_padding_x = max_padding_x
        self.min_padding_y = min_padding_y
        self.max_padding_y = max_padding_y
        self.min_font_size = min_font_size
        self.max_font_size = max_font_size
        self.text_angle_std = text_angle_std
        self.delimiter = delimiter
        self.grayscale = grayscale
        self.background_overlay = background_overlay
        self.output_dir = output_dir

        self.backgrounds = []
        self.background_types = [BackgroundType.COLOR, BackgroundType.NOISE]
        if background_dir:
            self.background_types.append(BackgroundType.IMAGE)
            self.backgrounds = [path for path in walk_dir(background_dir)
                                if path.endswith('.jpg')]

        # Read fonts from the `fonts_dir`.
        self.fonts = [os.path.join(fonts_dir, font) for font in
                      os.listdir(fonts_dir) if font.endswith('.ttf')]

        # Create the `output_dir` if it was specified.
        if self.output_dir:
            os.makedirs(self.output_dir, exist_ok=True)

    def resize(self, image: Image.Image) -> Image.Image:
        if self.height and not self.width:
            width = int(image.width * self.height / image.height)
            height = self.height
            return image.resize((width, height), BILINEAR)

        elif self.width and not self.height:
            height = int(image.height * self.width / image.width)
            width = self.width
            return image.resize((width, height), BILINEAR)

        elif self.width and self.height:
            return image.resize((self.width, self.height), BILINEAR)

    def generate_instance(self) -> Tuple[Image.Image, str]:
        text = self.generate_text()
        foreground = self.generate_foreground(text)
        background = self.generate_background(*foreground.size)
        text_color = self.get_text_color()
        image = self.merge(background, foreground, text_color)
        image = self.resize(image)
        if self.grayscale:
            image = image.convert('L')
        if self.output_dir:
            filename = self.generate_filename(text)
            path = os.path.join(self.output_dir, filename)
            image.save(path, 'PNG')
        return image, text

    @staticmethod
    def generate_filename(text: str) -> str:
        """
        Creates a filename for an instance that contains the given text.

        :param text: str
        :return: str
        """
        return '{}_{}.png'.format(
            uuid.uuid4().hex[:6],
            base64.b64encode(bytes(text, 'utf-8'), b'+-').decode('utf-8')
        )

    @staticmethod
    def merge(background: Image.Image,
              foreground: Image.Image,
              text_color: Tuple[int, int, int] = (0, 0, 0)) -> Image.Image:
        """
        Merges the `foreground` and `background` images together.

        :param background: Image.Image
        :param foreground: Image.Image
        :param text_color: Tuple[int, int, int]
        :return: Image.Image
        """
        assert background.size == foreground.size
        image = background.copy()
        image.paste(text_color, (0, 0, *foreground.size), foreground)
        return image

    def generate_text(self) -> str:
        """
        Generates a random string to be written on an image. Recursively calls
        itself until a text string is generated with non-zero length.

        :return: str
        """
        length = random.randint(self.min_num_tokens, self.max_num_tokens)
        text = self.delimiter.join(
            random.choice(self.vocabulary.alphabet) for _ in range(length))
        return text.strip() or self.generate_text()

    def generate_foreground(self, text: str) -> Image.Image:
        """
        Generates an image with the given `text` on it. The font, displacement,
        etc. are determined randomly.

        :param text: str
        :return: Image.Image
        """
        font_size = random.randint(self.min_font_size, self.max_font_size)
        font_file = random.choice(self.fonts)
        font = ImageFont.truetype(font_file, font_size)
        letter_spacing = random.randint(self.min_letter_spacing,
                                        self.max_letter_spacing)
        text_width, text_height = font.getsize(text)
        if letter_spacing:
            text_width = sum([font.getsize(char)[0] for char in text])
            text_width += letter_spacing * (len(text) - 1)
        padding_x = random.randint(self.min_padding_x, self.max_padding_x)
        padding_y = random.randint(self.min_padding_y, self.max_padding_y)
        height = text_height + padding_y
        width = text_width + padding_x

        image = Image.new('L', (width, height))
        offset_x = random.randint(*sorted([0, padding_x]))
        offset_y = random.randint(*sorted([0, padding_y]))
        draw = ImageDraw.Draw(image)
        if letter_spacing:
            for char in text:
                draw.text((offset_x, offset_y), char, 255, font=font)
                char_width, _ = font.getsize(char)
                offset_x += char_width + letter_spacing
        else:
            draw.text((offset_x, offset_y), text, 255, font=font)
        angle = random.gauss(0, self.text_angle_std)
        return image.rotate(angle, expand=True)

    def generate_background(
            self,
            width: int,
            height: int,
            background_type: Optional[BackgroundType] = None
    ) -> Image.Image:
        """
        Generates a background image of type `background_type` with the given
        `width` and `height` to paste the text upon. If no `background_type` is
        specified, a random background type is chosen.

        :param width: int
        :param height: int
        :param background_type: Optional[BackgroundType]
        :return: Image.Image
        """
        if not background_type:
            background_type = random.choice(self.background_types)

        if background_type == BackgroundType.COLOR:
            return self.generate_background_color(width, height)

        if background_type == BackgroundType.NOISE:
            return self.generate_background_noise(width, height)

        if background_type == BackgroundType.IMAGE:
            return self.generate_background_image(width, height)

    def generate_background_color(self,
                                  width: int,
                                  height: int) -> Image.Image:
        min_v = 255 - self.MAX_BACKGROUND_DARKNESS * self.color_variation
        h = random.randint(0, 255)
        s = random.randint(0, 255)
        v = random.randint(min_v, 255)
        image = Image.new('HSV', (width, height), (h, s, v))
        image = image.convert('RGB')
        return image

    def generate_background_noise(self,
                                  width: int,
                                  height: int) -> Image.Image:
        std = self.MAX_NOISE_STD * self.color_variation
        mean = 255 - 2 * std
        noise = np.random.normal(mean, std, (height, width, 3))
        noise = np.clip(noise, 0, 255)
        noise = noise.astype(np.uint8)
        image = Image.fromarray(noise, 'RGB')
        return image

    def generate_background_image(self,
                                  width: int,
                                  height: int) -> Image.Image:
        image = Image.open(random.choice(self.backgrounds))
        image = image.convert('RGB')
        image = image.resize((width, height), BILINEAR)
        if self.background_overlay > 0:
            overlay = self.generate_background_color(width, height)
            overlay.putalpha(int(255 * self.background_overlay))
            image.paste(overlay, (0, 0), overlay)
        return image

    def get_text_color(self) -> Tuple[int, int, int]:
        """
        Returns a random RGB text color.

        :return: Tuple[int, int, int]
        """
        max_value = self.MAX_TEXT_COLOR_VALUE * self.color_variation
        return (random.randint(0, max_value),
                random.randint(0, max_value),
                random.randint(0, max_value))
