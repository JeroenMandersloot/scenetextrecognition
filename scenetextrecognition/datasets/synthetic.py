import abc
from typing import Iterator, Optional
from typing import List
from typing import Tuple

import numpy as np
import tensorflow as tf
from PIL import Image
from cuddle.data.vocabulary import Vocabulary

from scenetextrecognition.augmenters.augmenter import Augmenter
from scenetextrecognition.datasets.str import SceneTextRecognitionDataset
from scenetextrecognition.synthetic.stream import SyntheticDataStream


class SyntheticDataset(SceneTextRecognitionDataset):
    def __init__(self,
                 source: SyntheticDataStream,
                 vocabulary: Vocabulary,
                 max_length: int,
                 grayscale: bool,
                 augmenter: Optional[Augmenter] = None):
        self.source = source
        self.vocabulary = vocabulary
        self.max_length = max_length
        self.grayscale = grayscale
        self.augmenter = augmenter
        super().__init__()

    def get_input(self, x: Tuple[Image.Image, str]) -> List[tf.Tensor]:
        return [self.get_encoder_input(x), self.get_decoder_input(x)]

    def get_target(self, x: Tuple[Image.Image, str]) -> tf.Tensor:
        return self.vocabulary.ohe(
            x[1], self.max_length, add_start=False, add_end=True)

    def get_encoder_input(self, x: Tuple[Image.Image, str]) -> tf.Tensor:
        image, _ = x
        image = np.array(image)
        if self.augmenter:
            image = self.augmenter.augment(image)
        if self.grayscale:
            image = image[:, :, None]
        image = image / 127.5 - 1
        return tf.convert_to_tensor(image, dtype=tf.float32)

    def get_width(self, x: Tuple[Image.Image, str]) -> int:
        return x[0].width

    def __len__(self) -> int:
        return len(self.source)

    def __iter__(self) -> Iterator[Tuple[Image.Image, str]]:
        return iter(self.source)

    @abc.abstractmethod
    def get_decoder_input(self, x: Tuple[Image.Image, str]):
        raise NotImplementedError


class SyntheticTrainDataset(SyntheticDataset):
    def get_decoder_input(self, x: Tuple[Image.Image, str]):
        return self.vocabulary.ohe(
            x[1], self.max_length, add_start=True, add_end=True)


class SyntheticTestDataset(SyntheticDataset):
    def get_decoder_input(self, x: Tuple[Image.Image, str]):
        return self.vocabulary.ohe('', add_start=True)
