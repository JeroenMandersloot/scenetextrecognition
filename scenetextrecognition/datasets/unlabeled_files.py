import os
from typing import Iterator, Union, List

import cv2
import numpy as np
import tensorflow as tf
from cuddle.data.vocabulary import Vocabulary

from scenetextrecognition.datasets.str import SceneTextRecognitionDataset


class UnlabeledFiles(SceneTextRecognitionDataset):
    def __init__(self,
                 root: str,
                 vocabulary: Vocabulary,
                 height: int,
                 max_length: int,
                 grayscale: bool):
        self.root = root
        self.vocabulary = vocabulary
        self.height = height
        self.max_length = max_length
        self.grayscale = grayscale
        self.data = os.listdir(root)
        super().__init__()

    def read(self, x) -> np.ndarray:
        image = cv2.imread(os.path.join(self.root, x))
        height, width, *_ = image.shape
        aspect_ratio = self.height / height
        image = cv2.resize(image, (int(aspect_ratio * width), self.height))
        if self.grayscale:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        return image

    def get_path(self, x: str):
        return os.path.join(self.root, x)

    def get_width(self, x: str) -> int:
        return self.read(x).shape[1]

    def get_input(self, x: str) -> List[tf.Tensor]:
        return [self.get_encoder_input(x), self.get_decoder_input(x)]

    def get_encoder_input(self, x: str) -> tf.Tensor:
        image = self.read(x)
        if self.grayscale:
            image = image[:, :, None]
        image = image / 127.5 - 1
        return tf.convert_to_tensor(image, dtype=tf.float32)

    def get_decoder_input(self, x):
        return self.vocabulary.ohe('', add_start=True)

    def get_target(self, x) -> Union[tf.Tensor, List[tf.Tensor]]:
        return tf.constant(0)

    def __len__(self) -> int:
        return len(self.data)

    def __iter__(self) -> Iterator[str]:
        return iter(self.data)
