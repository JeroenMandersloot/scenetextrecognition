import os
from typing import Tuple, List, Iterator, Any

import cv2
import numpy as np
import tensorflow as tf
from cuddle.data.vocabulary import Vocabulary

from scenetextrecognition.datasets.str import SceneTextRecognitionDataset


class EastDataset(SceneTextRecognitionDataset):
    def __init__(self,
                 root: str,
                 vocabulary: Vocabulary,
                 height: int,
                 max_length: int,
                 grayscale: bool):
        self.root = root
        self.vocabulary = vocabulary
        self.height = height
        self.max_length = max_length
        self.grayscale = grayscale
        self.data = self._parse_data()
        super().__init__()

    def _parse_data(self) -> List[Tuple[str, str]]:
        data = []
        annotations_path = os.path.join(self.root, 'sample.txt')
        with open(annotations_path, 'r') as f:
            for line in f.readlines():
                line = line.strip()
                filename, label = line.split(';')
                data.append((filename, label))
        return data

    def read(self, x) -> np.ndarray:
        filename, _ = x
        image = cv2.imread(os.path.join(self.root, filename))
        height, width, *_ = image.shape
        aspect_ratio = self.height / height
        image = cv2.resize(image, (int(aspect_ratio * width), self.height))
        if self.grayscale:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        return image

    def get_width(self, x: Any) -> int:
        image = self.read(x)
        return image.shape[1]

    def get_input(self, x: Tuple[str, str]) -> List[tf.Tensor]:
        return [self.get_encoder_input(x), self.get_decoder_input(x)]

    def get_target(self, x: Tuple[str, str]) -> tf.Tensor:
        return self.vocabulary.ohe(
            x[1], self.max_length, add_start=False, add_end=True)

    def get_encoder_input(self, x: Tuple[str, str]) -> tf.Tensor:
        image = self.read(x)
        if self.grayscale:
            image = image[:, :, None]
        image = image / 127.5 - 1
        return tf.convert_to_tensor(image, dtype=tf.float32)

    def __len__(self) -> int:
        return len(self.data)

    def __iter__(self) -> Iterator[Tuple[str, str]]:
        return iter(self.data)

    def get_decoder_input(self, x: Tuple[str, str]):
        return self.vocabulary.ohe('', add_start=True)
