from abc import abstractmethod
from collections import defaultdict
from typing import Any, Tuple
from typing import Generator, Union, List, Dict

import tensorflow as tf
from cuddle.data import Dataset
from cuddle.data import MergedDataset


class SceneTextRecognitionDataset(Dataset):
    def __init__(self):
        self._buckets: Dict[int, List] = defaultdict(list)

    def batches(self, batch_size: int) -> Generator[
        Tuple[Union[tf.Tensor, List[tf.Tensor]],
              Union[tf.Tensor, List[tf.Tensor]]], None, None]:

        for x in self:
            width = self.get_width(x)
            self._buckets[width].append(x)
            if len(self._buckets[width]) == batch_size:
                yield self.create_batch(self._buckets[width])
                del self._buckets[width]

    @abstractmethod
    def get_width(self, x: Any) -> int:
        """
        Returns the width for the image corresponding to instance `x`.

        :param x: Any
        :return: int
        """
        raise NotImplementedError


class MergedSceneTextRecognitionDataset(MergedDataset,
                                        SceneTextRecognitionDataset):
    def __init__(self, datasets: List[Dataset], alternate: bool = False):
        SceneTextRecognitionDataset.__init__(self)
        MergedDataset.__init__(self, datasets, alternate)

    def get_width(self, x: Tuple[SceneTextRecognitionDataset, Any]) -> int:
        return x[0].get_width(x[1])
