import abc
import os
from typing import Iterator, Any, List, Dict

import cv2
import numpy as np
import tensorflow as tf
from cuddle.data.vocabulary import Vocabulary

from scenetextrecognition.datasets.str import SceneTextRecognitionDataset
from scenetextrecognition.util import walk_dir


class MJSynthDataset(SceneTextRecognitionDataset):
    def __init__(self,
                 root: str,
                 vocabulary: Vocabulary,
                 max_length: int,
                 height: int,
                 grayscale: bool = False):
        self.root = root
        self.vocabulary = vocabulary
        self.max_length = max_length
        self.height = height
        self.grayscale = grayscale
        super().__init__()

    def read(self, path: str) -> np.ndarray:
        image = cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2RGB)
        height, width, _ = image.shape
        aspect_ratio = self.height / height
        new_height = int(self.height)
        new_width = int(aspect_ratio * width)
        image = cv2.resize(image, (new_width, new_height))
        if new_width < new_height:
            padding = np.zeros(
                (new_height, new_height - new_width, *image.shape[2:]))
            image = np.concatenate([image, padding], axis=1)
        if self.grayscale:
            image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)[:, :, None]
        image = image / 127.5 - 1
        return image

    def get_width(self, x: Dict[str, Any]) -> int:
        return x['width']

    def get_input(self, x: Dict[str, Any]) -> List[tf.Tensor]:
        return [self.get_encoder_input(x), self.get_decoder_input(x)]

    @staticmethod
    def get_encoder_input(x: Dict[str, Any]):
        return tf.convert_to_tensor(x['image'], dtype=tf.float32)

    @abc.abstractmethod
    def get_decoder_input(self, x: Dict[str, Any]):
        raise NotImplementedError

    def get_target(self, x: Dict[str, Any]) -> tf.Tensor:
        return self.vocabulary.ohe(x['label'], self.max_length, add_end=True)

    def __iter__(self) -> Iterator[Any]:
        return filter(self.filter_func, map(self._parse_path,
                                            walk_dir(self.root,
                                                     shuffle=True)))

    def _parse_path(self, path):
        try:
            image = self.read(path)
        except:
            return None
        return {
            'path': path,
            'image': image,
            'width': image.shape[1],
            'label': os.path.splitext(os.path.basename(path))[0].split('_')[1]
        }

    @abc.abstractmethod
    def filter_func(self, x: Dict[str, Any]) -> bool:
        raise NotImplementedError


class MJSynthTrain(MJSynthDataset):
    def filter_func(self, x: Dict[str, Any]):
        if x:
            return '1429/7' not in x['path']
        return False

    def get_decoder_input(self, x: Dict[str, Any]):
        return self.vocabulary.ohe(
            x['label'], self.max_length, add_start=True, add_end=True)

    def __len__(self) -> int:
        return int(9e6)


class MJSynthTest(MJSynthDataset):
    def filter_func(self, x: Dict[str, Any]):
        if x:
            return '1429/7' in x['path']
        return False

    def get_decoder_input(self, x: Dict[str, Any]):
        return self.vocabulary.ohe('', add_start=True)

    def __len__(self) -> int:
        return 169
