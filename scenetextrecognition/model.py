import math
import os
from contextlib import contextmanager
from typing import List, Union, Optional, Tuple

import cv2
import numpy as np
import tensorflow as tf
from cuddle.data.vocabulary import Vocabulary
from cuddle.models import Model
from tensorflow.keras import Sequential
from tensorflow.keras.losses import categorical_crossentropy
from tensorflow.keras.layers import (
    Input, Conv2D, Dense, MaxPool2D, BatchNormalization, Dropout, LSTM)


class SceneTextRecognitionModel(Model):
    def __init__(self,
                 output_dir: Optional[str],
                 vocabulary: Vocabulary,
                 max_length: int,
                 height: int,
                 units: int,
                 grayscale: bool):
        self.vocabulary = vocabulary
        self.max_length = max_length
        self.height = height
        self.units = units
        self.grayscale = grayscale

        # Build the models.
        num_channels = 1 if self.grayscale else 3
        self.encoder_input = Input(shape=(self.height, None, num_channels))
        self.decoder_input = Input(shape=(None, len(self.vocabulary)))
        self.encoder = Encoder(self.height, self.units)
        self.attention = Attention(self.units)
        self.decoder = Decoder(self.units)
        self.output = Output(self.units, len(self.vocabulary))
        self.training_model = self.build_training_model()
        self.inference_model = self.build_inference_model()
        self.visualization_model = self.build_visualization_model()
        self._active_model = None
        super().__init__(output_dir=output_dir)

    def build_training_model(self) -> tf.keras.Model:
        """
        Builds and returns the keras model used during training.

        :return: tf.keras.Model
        """
        encoder_output = self.encoder(self.encoder_input)
        positional_encoding = self.get_positional_encoding()
        context_vectors, _ = self.attention(
            self.decoder_input, positional_encoding, encoder_output)
        x = tf.concat([self.decoder_input, context_vectors], axis=2)
        decoder_output, _, _ = self.decoder(x, initial_state=None)
        scores = self.output(decoder_output)
        return tf.keras.Model([self.encoder_input, self.decoder_input], scores)

    def build_inference_model(self):
        """
        Builds and returns the keras model used during inference.

        :return: tf.keras.Model
        """
        outputs = []
        output = self.decoder_input
        encoder_output = self.encoder(self.encoder_input)
        initial_state = None
        for i in range(self.max_length):
            positional_encoding = self.get_positional_encoding(i)
            context_vectors, _ = self.attention(
                output, positional_encoding, encoder_output)
            x = tf.concat([output, context_vectors], axis=2)
            decoder_output, state_h, state_c = self.decoder(x, initial_state)
            initial_state = [state_h, state_c]
            output = self.output(decoder_output)
            outputs.append(output)
        scores = tf.concat(outputs, axis=1)
        return tf.keras.Model([self.encoder_input, self.decoder_input], scores)

    def build_visualization_model(self):
        """
        Builds and returns a keras visualization model for inference. It
        differs from the default inference model in that it also outputs the
        attention weights, which can be used for further visualization.

        :return: tf.keras.Model
        """
        attention = []
        outputs = []
        output = self.decoder_input
        encoder_output = self.encoder(self.encoder_input)
        initial_state = None
        for i in range(self.max_length):
            positional_encoding = self.get_positional_encoding(i)
            context_vectors, attention_weights = self.attention(
                output, positional_encoding, encoder_output)
            x = tf.concat([output, context_vectors], axis=2)
            decoder_output, state_h, state_c = self.decoder(x, initial_state)
            initial_state = [state_h, state_c]
            output = self.output(decoder_output)
            outputs.append(output)
            attention.append(attention_weights)
        scores = tf.concat(outputs, axis=1)
        attention = tf.concat(attention, axis=1)
        return tf.keras.Model(
            [self.encoder_input, self.decoder_input], [scores, attention])

    def get_positional_encoding(self,
                                position: Optional[int] = None) -> tf.Tensor:
        """
        Returns a 3D tensor with positional encodings for the encoder and
        decoder outputs. If `position` is omitted, the shape of the returned
        tensor will be (batch_size, self.max_length, self.max_length). If
        `position` is specified, the positional encoding for that position is
        returned, so the shape will be (batch_size, 1, self.max_length).

        Every row (the last axis/dimension) in the resulting tensor is a one-
        hot encoded vector where the 1-entry represents the position. Hence why
        the size of the last dimension is equal to self.max_length.

        :param position: Optional[int]
        :return: tf.Tensor, a 3D tensor with the positional encodings
        """
        batch_shape = tf.expand_dims(self.get_batch_size(), 0)
        eye = tf.eye(self.max_length, batch_shape=batch_shape)
        if position is not None:
            return tf.expand_dims(eye[:, position, :], axis=1)
        return eye

    def get_batch_size(self):
        """
        Returns the batch size (i.e. first dimension) for the data being
        passed through the model at runtime.

        :return: tf.Tensor
        """
        return tf.shape(self.decoder_input)[0]

    def keras(self) -> tf.keras.Model:
        if self._active_model:
            return self._active_model
        if self.is_training():
            return self.training_model
        return self.inference_model

    def compute_loss(
            self,
            targets: tf.Tensor,
            predictions: Union[tf.Tensor, List[tf.Tensor]]
    ) -> tf.Tensor:
        return tf.reduce_mean(categorical_crossentropy(targets, predictions))

    def visualize_attention(
            self,
            x: List[tf.Tensor],
            output_dir: Optional[str] = None
    ) -> List[Tuple[str, np.ndarray]]:
        """
        Applies the model to the single instance `x` and visualizes which parts
        of the image the model paid attention to during decoding.

        Args:
            x:  Input for the model, i.e. a batch containing a single instance.
                Since the model takes two inputs `x` should have two tensors.
                The first `Tensor` is the encoder input, representing the
                source image with shape (1, height, width, channels).
                The second `Tensor` is the decoder input and should generally
                represent the <SOS> symbol with shape (1, 1, vocab_size).
            output_dir: An optional output directory to store the results.

        Returns:
            A list of the attention paid to the image for each predicted token
            individually. Each result is a tuple containing the predicted token
            and a copy of the source image overlaid with a heatmap to highlight
            the important regions for that prediction.
        """

        # Extract the image datasets from the model input.
        image = cv2.normalize(
            x[0][0].numpy(), None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)

        if self.grayscale:
            image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)

        # Determine the resolution of each attention region.
        original_width = image.shape[1]
        encoded_width = self.encoder.get_encoded_width(original_width)

        # Apply the model to `x` to obtain the predicted text and corresponding
        # attention weights.
        with self.active_model(self.visualization_model):
            predictions, attention_weights = self.predict(x, training=False)
        text = self.vocabulary.ohd(predictions[0])

        # For each predicted token, generate a heatmap and plot it on top of
        # the original source image.
        results = []
        receptive_columns = self.encoder.get_receptive_columns(original_width)
        for i, c in enumerate(text):
            # Initialize the heatmap with all zeros, then sum the attention
            # weights for each pixel.
            heatmap = np.zeros(image.shape[:2])
            for j in range(encoded_width):
                start, end = receptive_columns[j]
                heatmap[:, start:end] += attention_weights[0, i, j]

            # Overlay the heatmap on top of the original source image.
            heatmap = (255 * heatmap).astype(np.uint8)
            heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_WINTER)
            attention_image = cv2.addWeighted(image, 0.4, heatmap, 0.6, 0)
            results.append((c, attention_image))

        # If an output directory was specified, write all the results to disk.
        if output_dir:
            os.makedirs(output_dir, exist_ok=True)
            # First, store the original source image for reference.
            output_path = os.path.join(output_dir, f'{"".join(text)}.png')
            cv2.imwrite(output_path, image)
            for i, (c, attention_image) in enumerate(results):
                # Then, store the attention images of the individual tokens.
                output_path = os.path.join(output_dir, f'{i}_{c}.png')
                cv2.imwrite(output_path, attention_image)
        return results

    @contextmanager
    def active_model(self, model: tf.keras.Model):
        previous_model = self._active_model
        self._active_model = model
        yield None
        self._active_model = previous_model


class Encoder:
    def __init__(self, height, units):
        self.layers = [
            Conv2D(64, (3, 3), padding='same', activation='relu'),
            MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='same'),
            BatchNormalization(),
            Conv2D(128, (3, 3), padding='same', activation='relu'),
            MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='same'),
            BatchNormalization()
        ]

        encoded_height = math.ceil(height / 4)
        while encoded_height > 1:
            encoded_height = math.ceil(encoded_height / 2)
            num_filters = height // encoded_height * 32
            self.layers += [
                Conv2D(num_filters, (3, 3), padding='same', activation='relu'),
                BatchNormalization(),
                Conv2D(num_filters, (3, 3), padding='same', activation='relu'),
                MaxPool2D(pool_size=(2, 1), strides=(2, 1), padding='same'),
                BatchNormalization()
            ]

        # Add a Dropout layer at the end of our convolutional stack.
        self.layers.append(Dropout(rate=0.5))

        # We first encode the visual features of the input through a CNN. The
        # CNN architecture squashes the height of each input image to 1 and
        # then squeezes out this dimension.
        self.cnn = Sequential(self.layers)

        # The visual encodings are then passed through a bidirectional LSTM.
        self.forward = LSTM(units, return_sequences=True)
        self.backward = LSTM(units, return_sequences=True, go_backwards=True)

        # This projection layer projects the concatenated encoder states of the
        # forward and backward LSTMs back down to a dimensionality of `units`
        # (from 2 * `units` due to the concatenation).
        self.projection = Dense(units, activation=None, use_bias=False)

    def __call__(self, encoder_input):
        output = self.cnn(encoder_input)
        output = tf.squeeze(output, axis=1)
        forward_output = self.forward(output)
        backward_output = tf.reverse(self.backward(output), axis=[1])
        output = tf.concat([forward_output, backward_output], axis=2)
        return self.projection(output)

    def get_receptive_columns(self, original_width) -> List[Tuple[int, int]]:
        """
        Returns a list with a number of elements equal to the encoder output
        size. Each i-th element specifies the range of columns in the original
        source image that belong to the receptive field of the i-th encoder
        output. Note that the columns for different elements may overlap.

        One other aspect to take into consideration is that layers with 'same'
        padding distort the receptive fields of the pixels on the borders.
        Their receptive fields are asymmetric since padding is added to one
        side.

        :param original_width: int
        :return: List[Tuple[int, int]]
        """
        receptive_step_size = self.get_horizontal_receptive_step_size()
        receptive_offset = self.get_first_column_receptive_offset()
        receptive_width = self.get_receptive_width()
        encoded_width = self.get_encoded_width(original_width)
        results = []
        for i in range(encoded_width):
            start = int(receptive_offset + i * receptive_step_size)
            end = int(start + receptive_width)
            results.append((max(start, 0), min(end, original_width)))
        return results

    def get_horizontal_receptive_step_size(self) -> int:
        """
        Returns the step size between the x-coordinates of the receptive fields
        in the original image of two neighbouring pixels in the final
        convolutional feature map. These are determined by pooling layers.

        :return: int
        """
        step_size = 1
        for layer in self.layers:
            if isinstance(layer, MaxPool2D):
                assert layer.strides == layer.pool_size
                step_size *= layer.pool_size[1]
        return step_size

    def get_first_column_receptive_offset(self) -> int:
        """
        Layers with 'same' padding distort the receptive fields of the pixels
        on the borders. Their receptive fields are asymmetric since padding is
        added to one side. This method returns a (negative) offset to correct
        for this effect.

        :return: int
        """
        offset = 0
        for layer in reversed(self.layers):
            if isinstance(layer, MaxPool2D):
                offset *= layer.pool_size[1]
            elif isinstance(layer, Conv2D):
                if layer.padding == 'same':
                    offset -= math.floor(layer.kernel_size[1] / 2)
        return offset

    def get_receptive_width(self) -> int:
        """
        Returns the receptive width per "pixel" in the final convolutional
        feature map. The receptive width is the width of the receptive field,
        i.e. the region of pixels in the original image that are involved in
        computing the pixel value in the final feature map.

        The receptive field for each element can be computed by traversing the
        CNN architecture backwards and repeatedly updating the dimensions of
        the receptive field. Each convolutional layer adds half its kernel size
        rounded up (assuming strides of (1, 1)). Each pooling layer multiplies
        the receptive field by its pool size (assuming strides of equal size).

        For example, consider a sequential architecture with these layers:

            (1) Conv2D(filters=..., kernel_size=(3, 3))
            (2) MaxPool2D(pool_size=(2, 2), strides=(2, 2))
            (3) Conv2D(filters=..., kernel_size=(3, 3))
            (4) MaxPool2D(pool_size=(2, 1), strides=(2, 1))

        The receptive field on the x-axis for a single pixel in the final
        feature map would then be given by:

           (x1)     (+2)     (x2)     (+2)
        1 -----> 1 -----> 3 -----> 6 -----> 8

        Explanation:
            (1) We traverse the architecture backwards, so the first layer to
                consider is the last MaxPool2D layer with a pool_size of
                (2, 1). That means the receptive field on the x-axis is
                multiplied by a factor of 1 (remains unchanged).
            (2) Next we have a Conv2D layer with a kernel_size of (3, 3). We
                add 3 / 2 rounded up to the receptive field to get 1 + 2 = 3.
            (3) Then we have another MaxPool2D layer, now with a pool_size of
                (2, 2). We therefore multiply our receptive field by a factor
                of 2 to get 3 * 2 = 6.
            (4) Finally we have another Conv2D layer with the same kernel size
                as before. We again add 2 and obtain a receptive field of
                6 + 2 = 8.

        :return: int
        """
        width = 1
        for layer in reversed(self.layers):
            if isinstance(layer, MaxPool2D):
                width *= layer.pool_size[1]
            elif isinstance(layer, Conv2D):
                width += math.ceil(layer.kernel_size[1] / 2)
        return width

    def get_encoded_width(self, original_width: int) -> int:
        """
        Computes the width of the encoded image given its original width and
        the encoder architecture.

        :param original_width: int, the width of the input image
        :return: int, the width of the final convolutional feature map
        """
        width = original_width
        for layer in self.layers:
            if isinstance(layer, MaxPool2D):
                assert layer.strides == layer.pool_size
                width = math.ceil(width / layer.pool_size[1])
            elif isinstance(layer, Conv2D):
                if layer.padding == 'valid':
                    assert layer.strides == (1, 1)
                    width = width - math.ceil(layer.kernel_size[1] / 2)
        return width


class Attention:
    def __init__(self, units: int):
        self.projection_q = Dense(units, activation=None, use_bias=False)
        self.projection_k = Dense(units, activation=None, use_bias=False)

    def __call__(self, decoder_input, positional_encoding, encoder_output):
        queries = tf.concat([decoder_input, positional_encoding], axis=2)
        Q = self.projection_q(queries)
        K = self.projection_k(encoder_output)
        V = encoder_output

        logits = tf.matmul(Q, tf.transpose(K, perm=[0, 2, 1]))
        attention_weights = tf.nn.softmax(logits)
        context_vectors = tf.matmul(attention_weights, V)
        return [context_vectors, attention_weights]


class Decoder:
    def __init__(self, units):
        self.lstm = LSTM(units, return_sequences=True, return_state=True)

    def __call__(self, decoder_input, initial_state):
        return self.lstm(decoder_input, initial_state)


class Output:
    def __init__(self, units, vocab_size):
        self.hidden = Dense(units, activation='relu')
        self.dense = Dense(vocab_size, activation='softmax')

    def __call__(self, decoder_output):
        return self.dense(self.hidden(decoder_output))
