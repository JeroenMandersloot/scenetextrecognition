import os
import random
import re
from itertools import islice
from typing import List, Optional

import requests
from bs4 import BeautifulSoup
from cuddle.data.vocabulary import Vocabulary
from tqdm import tqdm


class WikipediaVocabulary(Vocabulary):
    def __init__(self,
                 languages: List[str],
                 num_tokens: int,
                 cache_file: Optional[str] = None,
                 verbose: bool = True):
        """
        Create a vocabulary consisting of random words from Wikipedia articles.

        You can specify a list of `languages` for which to retrieve words. Each
        time an article is crawled, its language will be drawn from this list
        uniformly at random.

        :param languages: List[str]
        :param num_tokens: int
        :param cache_file: Optional[str]
        :param verbose: bool
        """
        generator = map(
            self.random_capitalization, self.generate_words(languages))

        # Read from the cache, if it exists.
        cache = []
        if cache_file and os.path.exists(cache_file):
            with open(cache_file, 'r') as f:
                cache = [token.strip() for token in f.readlines()[:num_tokens]]

        # Determine how many tokens still need to be generated.
        num_to_generate = num_tokens - len(cache)

        # Generate remaining tokens.
        if verbose:
            print("Building Wikipedia vocabulary...")
            generator = tqdm(generator, total=num_to_generate)
        alphabet = cache + list(islice(generator, num_to_generate))

        # Cache the new alphabet, as long as it has been expanded.
        if cache_file and len(alphabet) > len(cache):
            with open(cache_file, 'w') as f:
                f.write('\n'.join(alphabet))
        super().__init__(list(alphabet))

    @staticmethod
    def random_capitalization(word):
        r = random.random()
        if r < 0.3:
            return word.capitalize()
        if r < 0.6:
            return word.upper()
        return word

    @staticmethod
    def generate_words(languages: List[str]):
        seen = set()
        while True:
            language = random.choice(languages)
            url = f'https://{language}.wikipedia.org/wiki/Special:Random'
            response = requests.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')
            for p in soup.findAll('p'):
                words = re.findall(r'\b[a-z0-9]+\b', p.getText())
                for word in words:
                    if word.isalnum() and word not in seen:
                        seen.add(word)
                        yield word
