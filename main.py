import os

import confidence
import tensorflow as tf
from confidence import Missing
from cuddle.data.vocabulary import Vocabulary
from tensorflow.python.training.adam import AdamOptimizer

from scenetextrecognition.augmenters.extensive import ExtensiveAugmenter
from scenetextrecognition.datasets.mjsynth import MJSynthTest, MJSynthTrain
from scenetextrecognition.datasets.str import MergedSceneTextRecognitionDataset
from scenetextrecognition.datasets.synthetic import SyntheticTrainDataset, \
    SyntheticTestDataset
from scenetextrecognition.model import SceneTextRecognitionModel
from scenetextrecognition.synthetic.data_generator import DataGenerator
from scenetextrecognition.synthetic.stream import \
    SyntheticDataStreamFromGenerator, SyntheticDataStreamFromDisk
from scenetextrecognition.util import fix_tensorflow_rtx
from scenetextrecognition.vocabularies.wikipedia import WikipediaVocabulary

if tf.__version__.startswith('1.'):
    tf.enable_eager_execution()

fix_tensorflow_rtx()


def create_wikipedia_vocabulary(config):
    return WikipediaVocabulary(
        languages=config.str.data.synthetic.wikipedia.languages,
        num_tokens=config.str.data.synthetic.wikipedia.vocab_size,
        cache_file=config.str.data.synthetic.wikipedia.cache_file
    )


def create_vocabulary(config):
    return Vocabulary(config.str.vocabulary)


def create_model(config, vocabulary):
    return SceneTextRecognitionModel(
        vocabulary=vocabulary,
        output_dir=config.str.model.output_dir,
        max_length=config.str.data.max_length,
        height=config.str.data.height,
        units=config.str.model.units,
        grayscale=config.str.data.grayscale
    )


def create_random_generator(config, vocabulary):
    return DataGenerator(
        vocabulary=vocabulary,
        height=config.str.data.height,
        width=config.str.data.width,
        min_num_tokens=config.str.data.synthetic.random.min_num_tokens,
        max_num_tokens=config.str.data.synthetic.random.max_num_tokens,
        grayscale=config.str.data.grayscale,
        fonts_dir=config.str.data.synthetic.fonts_dir,
        min_letter_spacing=config.str.data.synthetic.min_letter_spacing,
        max_letter_spacing=config.str.data.synthetic.max_letter_spacing,
        min_font_size=config.str.data.synthetic.min_font_size,
        max_font_size=config.str.data.synthetic.max_font_size,
        min_padding_x=config.str.data.synthetic.min_padding_x,
        max_padding_x=config.str.data.synthetic.max_padding_x,
        min_padding_y=config.str.data.synthetic.min_padding_y,
        max_padding_y=config.str.data.synthetic.max_padding_y,
        color_variation=config.str.data.synthetic.color_variation,
        background_dir=config.str.data.synthetic.background_dir,
        background_overlay=config.str.data.synthetic.background_overlay,
        delimiter=''
    )


def create_wikipedia_generator(config, vocabulary):
    return DataGenerator(
        vocabulary=vocabulary,
        height=config.str.data.height,
        width=config.str.data.width,
        min_num_tokens=config.str.data.synthetic.wikipedia.min_num_tokens,
        max_num_tokens=config.str.data.synthetic.wikipedia.max_num_tokens,
        min_letter_spacing=config.str.data.synthetic.min_letter_spacing,
        max_letter_spacing=config.str.data.synthetic.max_letter_spacing,
        min_font_size=config.str.data.synthetic.min_font_size,
        max_font_size=config.str.data.synthetic.max_font_size,
        min_padding_x=config.str.data.synthetic.min_padding_x,
        max_padding_x=config.str.data.synthetic.max_padding_x,
        min_padding_y=config.str.data.synthetic.min_padding_y,
        max_padding_y=config.str.data.synthetic.max_padding_y,
        color_variation=config.str.data.synthetic.color_variation,
        grayscale=config.str.data.grayscale,
        fonts_dir=config.str.data.synthetic.fonts_dir,
        background_dir=config.str.data.synthetic.background_dir,
        background_overlay=config.str.data.synthetic.background_overlay,
        delimiter=' '
    )


def create_augmenter(config):
    return ExtensiveAugmenter() if config.str.training.augment else None


def main(config):
    # Read config variables.
    batch_size = config.str.training.batch_size
    epochs = config.str.training.epochs
    learning_rate = config.str.training.learning_rate
    save_interval = config.str.training.save_interval
    height = config.str.data.height
    validation_dir = config.str.data.validation.path
    test_dir = config.str.data.test.path
    mjsynth_root = config.str.data.mjsynth.root
    max_length = config.str.data.max_length
    grayscale = config.str.data.grayscale

    vocabulary = create_vocabulary(config)
    wikipedia_vocabulary = create_wikipedia_vocabulary(config)
    augmenter = create_augmenter(config)
    model = create_model(config, vocabulary)
    random_generator = create_random_generator(config, vocabulary)
    wikipedia_generator = create_wikipedia_generator(
        config, wikipedia_vocabulary)

    # Create our synthetic train dataset.
    synthetic_train_dataset_random = SyntheticTrainDataset(
        source=SyntheticDataStreamFromGenerator(
            generator=random_generator,
            num_instances=config.str.data.synthetic.random.num_instances
        ),
        vocabulary=vocabulary,
        max_length=max_length,
        grayscale=grayscale,
        augmenter=augmenter
    )

    synthetic_train_dataset_wikipedia = SyntheticTrainDataset(
        source=SyntheticDataStreamFromGenerator(
            generator=wikipedia_generator,
            num_instances=config.str.data.synthetic.wikipedia.num_instances
        ),
        vocabulary=vocabulary,
        max_length=max_length,
        grayscale=grayscale,
        augmenter=augmenter
    )

    # Create the MJSynth training dataset.
    mjsynth_train_dataset = MJSynthTrain(
        os.path.join(mjsynth_root),
        vocabulary=vocabulary,
        max_length=max_length,
        height=height,
        grayscale=grayscale
    )

    # # Create a validation set with pre-generated synthetic instances.
    # synthetic_validation_dataset = SyntheticTestDataset(
    #     source=SyntheticDataStreamFromDisk(root=validation_dir),
    #     vocabulary=vocabulary,
    #     max_length=max_length,
    #     grayscale=grayscale,
    # )

    # Create a validation set with pre-generated synthetic instances.
    synthetic_test_dataset = SyntheticTestDataset(
        source=SyntheticDataStreamFromDisk(root=test_dir),
        vocabulary=vocabulary,
        max_length=max_length,
        grayscale=grayscale,
    )

    # Reserve part of the MJSynth dataset for validation as well.
    mjsynth_validation_dataset = MJSynthTest(
        os.path.join(mjsynth_root, '1429', '7'),
        vocabulary=vocabulary,
        max_length=max_length,
        height=height,
        grayscale=grayscale
    )

    # Merge all datasets that are to be used for training.
    train_dataset = MergedSceneTextRecognitionDataset(
        datasets=[
            synthetic_train_dataset_random,
            synthetic_train_dataset_wikipedia,
            mjsynth_train_dataset
        ],
        alternate=True
    )

    # Merge all datasets that are to be used for validation.
    validation_dataset = MergedSceneTextRecognitionDataset(
        datasets=[synthetic_test_dataset, mjsynth_validation_dataset]
    )

    # TODO: remove try/catch statement below in favor of something more stable.
    try:
        model.load()
    except (FileNotFoundError, OSError):
        print('No previous model found, starting from scratch...')

    try:
        optimizer = model.load_optimizer()
    except (FileNotFoundError, TypeError):
        print('No previous optimizer found, reinitializing...')
        optimizer = AdamOptimizer(learning_rate=learning_rate)

    # @model.on_step(interval=1)
    # def _(loss_, x_, y_, *args, **kwargs):
    #     loss_ = float(loss_.numpy())
    #     if loss_ > 0.15:
    #         difficult_dir = f'scratch/data/difficult/{model.global_step}'
    #         for i in range(batch_size):
    #             target = vocabulary.ohd(y_[i], delimiter='')
    #             out = f'{difficult_dir}/{target}'
    #             model.visualize_attention([x_[0][i:i+1], x_[1][i:i+1, 0:1]], out)

    @model.on_step(interval=10)
    def _(loss_, x_, y_, predictions_, *args, **kwargs):
        print(model.global_step, loss_)

    try:
        print("Started training")
        model.train(
            dataset=train_dataset,
            batch_size=batch_size,
            num_epochs=epochs,
            optimizer=optimizer,
            validation=dict(
                wikipedia=synthetic_test_dataset,
                mjsynth=mjsynth_validation_dataset
            ),
            save_interval=save_interval,
            verbose=False
        )
    except KeyboardInterrupt:
        loss = 0
        vocabulary = model.vocabulary
        for x, y in validation_dataset.batches(1):
            predictions = model.predict(x, training=False)
            target_word = vocabulary.ohd(y[0], delimiter='', )
            predicted_word = vocabulary.ohd(predictions[0], delimiter='')
            print(target_word, predicted_word)
            loss += model.compute_loss(y, predictions)
            output_dir = f'scratch/attention/{target_word}'
            os.makedirs(output_dir, exist_ok=True)
            model.visualize_attention(x, output_dir)
        print('Validation loss: ', loss / len(validation_dataset))


if __name__ == '__main__':
    main(confidence.loadf('scenetextrecognition.yaml', missing=Missing.error))
