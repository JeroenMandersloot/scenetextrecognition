import argparse
import os
import string
from tkinter import Frame, Tk, Label, TclError
from typing import List

import confidence
from PIL import Image, ImageFont, ImageDraw
from PIL.ImageTk import PhotoImage


class FontSelectorApp:
    def __init__(self, folder):
        self.root = Tk()
        self.frame = Frame()

        self._fonts = self.get_fonts(folder)
        self._text = string.ascii_letters + string.digits
        self._image = None  # Store image to prevent garbage collection.
        self._font_index = None
        self._image_panel = None
        self._font_panel = None
        self.build()

    @staticmethod
    def get_fonts(fonts_dir) -> List[str]:
        fonts = []
        font_files = os.listdir(fonts_dir)
        for font_file in sorted(font_files):
            if font_file.endswith('.ttf'):
                fonts.append(os.path.join(fonts_dir, font_file))
        return fonts

    def _update(self):
        if self._image_panel:
            self._image_panel.destroy()
        if self._font_panel:
            self._font_panel.destroy()
        font = ImageFont.truetype(self.current_font, 12)
        width, height = font.getsize(self._text)
        image = Image.new('RGB', (width, height), color=(255, 255, 255))
        draw = ImageDraw.Draw(image)
        draw.text((0, 0), self._text, (0, 0, 0), font)
        if not image.height or not image.width:
            self.delete()
        else:
            image = image.resize((image.width * 2, image.height * 2))
            self._image = PhotoImage(image)
            self._image_panel = Label(self.frame, image=self._image)
            self._image_panel.grid(row=1, column=1)
            self._font_panel = Label(
                self.frame,
                text='{} ({}/{})'.format(
                    self.current_font,
                    self._font_index + 1,
                    len(self._fonts)
                )
            )
            self._font_panel.grid(row=2, column=1)

    def next(self, *args):
        if self._font_index is None:
            self._font_index = 0
        else:
            self._font_index = (self._font_index + 1) % len(self._fonts)
        self._update()

    def prev(self, *args):
        if self._font_index is None:
            self._font_index = 0
        else:
            self._font_index = (self._font_index - 1) % len(self._fonts)
        self._update()

    def delete(self, *args):
        print(f'Deleting {self.current_font}...')
        os.unlink(self.current_font)
        self.next()

    def build(self):
        # Specify the window properties.
        self.root.geometry("1024x128")
        self.root.resizable(width=False, height=False)
        self.root.bind("<Delete>", self.delete)
        self.root.bind("<Right>", self.next)
        self.root.bind("<Left>", self.prev)
        self.frame.grid()
        self.next()

    def run(self):
        self.frame.mainloop()
        self.root.destroy()

    @property
    def current_font(self) -> str:
        return self._fonts[self._font_index]


if __name__ == '__main__':
    config = confidence.loadf('scenetextrecognition.yaml')
    default_folder = config.str.data.synthetic.fonts_dir
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--folder', type=str, default=default_folder)
    args = parser.parse_args()
    app = FontSelectorApp(**vars(args))
    try:
        app.run()
    except TclError:
        pass
