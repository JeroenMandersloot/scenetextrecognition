import random

import numpy as np
import pytest
import tensorflow as tf
from tensorflow.keras.layers import Conv2D, MaxPool2D, BatchNormalization

from scenetextrecognition.model import Encoder
from scenetextrecognition.util import fix_tensorflow_rtx

fix_tensorflow_rtx()

height = 45
encoder = Encoder(height, 100)


@pytest.mark.parametrize("width", [279, 280, 281, 282, 283, 284])
def test_get_encoded_width(width):
    dummy = tf.zeros((1, height, width, 3))
    encoded_shape = encoder.cnn(dummy).shape
    encoded_width = encoded_shape[2]
    assert encoded_width == encoder.get_encoded_width(width)


@pytest.mark.parametrize("width", [279, 280, 281, 282, 283, 284])
def test_get_receptive_columns(width):
    dummy = tf.zeros((1, height, width, 3))
    encoded_shape = encoder.cnn(dummy).shape
    encoded_width = encoded_shape[2]
    receptive_columns = encoder.get_receptive_columns(width)
    assert (len(receptive_columns) == encoded_width)
    assert (receptive_columns[0][0] == 0)
    assert (receptive_columns[-1][1] == width)


def test_layer_types():
    supported_types = [Conv2D, MaxPool2D, BatchNormalization]
    for layer in encoder.layers:
        assert any(map(lambda t: isinstance(layer, t), supported_types))


def test_get_receptive_width():
    receptive_width = encoder.get_receptive_width()
    dummy = np.random.rand(1, height, receptive_width + 1, 3)
    dummy[0, :, -1] = random.random()
    dummy_result_1 = encoder.cnn(dummy)
    dummy[0, :, -1] = random.random()
    dummy_result_2 = encoder.cnn(dummy)

    # The result for the first column should be the same, as the difference in
    # input has no way of propagating to that column.
    assert tf.reduce_all(
        tf.equal(dummy_result_1[:, :, 0], dummy_result_2[:, :, 0]))

    # The last column should be different though, as the difference in input
    # should have altered the computation.
    assert not tf.reduce_all(
        tf.equal(dummy_result_1[:, :, -1], dummy_result_2[:, :, -1]))


def test_get_receptive_width_with_offset():
    receptive_width = encoder.get_receptive_width()
    offset = encoder.get_first_column_receptive_offset()
    dummy = np.random.rand(1, height, offset + receptive_width + 1, 3)
    dummy[0, :, -1] = random.random()
    dummy_result_1 = encoder.cnn(dummy)
    dummy[0, :, -1] = random.random()
    dummy_result_2 = encoder.cnn(dummy)

    # The result for the first column should be the same, as the difference in
    # input has no way of propagating to that column.
    assert tf.reduce_all(
        tf.equal(dummy_result_1[:, :, 0], dummy_result_2[:, :, 0]))

    # The second column should be different though, as the difference in input
    # should have altered the computation.
    assert not tf.reduce_all(
        tf.equal(dummy_result_1[:, :, 1], dummy_result_2[:, :, 1]))
