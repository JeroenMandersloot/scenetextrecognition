import string

import numpy as np
from cuddle.data.vocabulary import Vocabulary

from scenetextrecognition.synthetic.data_generator import DataGenerator


def create_generator(**kwargs):
    return DataGenerator(
        fonts_dir='../fonts',
        vocabulary=Vocabulary(string.ascii_lowercase),
        height=32,
        min_length=1,
        max_length=20,
        delimiter='',
        **kwargs
    )


def test_background_image_is_rgb_1():
    generator = create_generator(grayscale=False)
    background = generator.generate_background(1, 1)
    assert background.mode == 'RGB'


def test_background_image_is_rgb_2():
    generator = create_generator(grayscale=True)
    background = generator.generate_background(1, 1)
    assert background.mode == 'RGB'


def test_background_image_is_rgb_3():
    generator = create_generator(grayscale=False,
                                 background_dir='resources/backgrounds')
    background = generator.generate_background(1, 1)
    assert background.mode == 'RGB'


def test_instance_is_rgb():
    generator = create_generator(grayscale=False)
    image, _ = generator.generate_instance()
    assert image.mode == 'RGB'


def test_instance_is_grayscale():
    generator = create_generator(grayscale=True)
    image, _ = generator.generate_instance()
    assert image.mode == 'L'
    assert len(np.array(image).shape) == 2
