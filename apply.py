import argparse
import os

import confidence
from cuddle.data.vocabulary import Vocabulary

from scenetextrecognition.datasets.unlabeled_files import UnlabeledFiles
from scenetextrecognition.model import SceneTextRecognitionModel


def create_vocabulary(config):
    return Vocabulary(config.str.vocabulary)


def create_model(config, vocabulary):
    return SceneTextRecognitionModel(
        vocabulary=vocabulary,
        output_dir=config.str.model.output_dir,
        max_length=config.str.data.max_length,
        height=config.str.data.height,
        units=config.str.model.units,
        grayscale=config.str.data.grayscale
    )


def main(config, folder):
    height = config.str.data.height
    max_length = config.str.data.max_length
    grayscale = config.str.data.grayscale
    vocabulary = create_vocabulary(config)
    model = create_model(config, vocabulary)

    model.load()

    dataset = UnlabeledFiles(
        root=folder,
        vocabulary=vocabulary,
        height=height,
        max_length=max_length,
        grayscale=grayscale
    )

    for filename in dataset:
        x, _ = dataset.create_batch([filename])
        predictions = model.predict(x, training=False)
        predicted_word = vocabulary.ohd(predictions[0], delimiter='')
        print(filename, predicted_word)
        output_dir = f'scratch/attention_unlabeled/{predicted_word}'
        os.makedirs(output_dir, exist_ok=True)
        model.visualize_attention(x, output_dir)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('folder', type=str)
    args = parser.parse_args()
    config = confidence.loadf('scenetextrecognition.yaml')
    main(config, **vars(args))
