import os
import shutil
import string

import confidence
import cv2
import numpy as np
from confidence import Missing
from cuddle.data.vocabulary import Vocabulary

from scenetextrecognition.augmenters.simple import SimpleAugmenter
from scenetextrecognition.synthetic.data_generator import DataGenerator

OUTPUT_DIR = 'scratch/tmp'


def create_random_generator(config, vocabulary):
    return DataGenerator(
        vocabulary=vocabulary,
        height=config.str.data.height,
        width=config.str.data.width,
        min_num_tokens=config.str.data.synthetic.random.min_num_tokens,
        max_num_tokens=config.str.data.synthetic.random.max_num_tokens,
        grayscale=config.str.data.grayscale,
        fonts_dir=config.str.data.synthetic.fonts_dir,
        min_letter_spacing=config.str.data.synthetic.min_letter_spacing,
        max_letter_spacing=config.str.data.synthetic.max_letter_spacing,
        min_font_size=config.str.data.synthetic.min_font_size,
        max_font_size=config.str.data.synthetic.max_font_size,
        min_padding_x=config.str.data.synthetic.min_padding_x,
        max_padding_x=config.str.data.synthetic.max_padding_x,
        min_padding_y=config.str.data.synthetic.min_padding_y,
        max_padding_y=config.str.data.synthetic.max_padding_y,
        color_variation=config.str.data.synthetic.color_variation,
        background_dir=config.str.data.synthetic.background_dir,
        background_overlay=config.str.data.synthetic.background_overlay,
        delimiter=''
    )


def create_augmenter(config):
    return SimpleAugmenter() if config.str.training.augment else None


def main(config):
    if os.path.exists(OUTPUT_DIR):
        shutil.rmtree(OUTPUT_DIR)

    vocabulary = Vocabulary(string.ascii_letters + string.digits)
    generator = create_random_generator(config, vocabulary)
    augmenter = create_augmenter(config)

    os.makedirs(OUTPUT_DIR, exist_ok=True)

    for _ in range(100):
        image, text = generator.generate_instance()
        image = np.array(image)
        image = augmenter.augment(image)
        path = os.path.join(OUTPUT_DIR, f'{text}.png')
        cv2.imwrite(path, image)


if __name__ == '__main__':
    main(confidence.loadf('scenetextrecognition.yaml', missing=Missing.error))
