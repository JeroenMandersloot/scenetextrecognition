import os

import confidence
import editdistance
from cuddle.data.vocabulary import Vocabulary

from scenetextrecognition.datasets.east import EastDataset
from scenetextrecognition.model import SceneTextRecognitionModel
from scenetextrecognition.util import fix_tensorflow_rtx

fix_tensorflow_rtx()


def compute_edit_distance(
        ground_truth,
        prediction,
        case_sensitive: bool = False,
        allow_sub_words: bool = False) -> float:
    if not case_sensitive:
        ground_truth = ground_truth.lower()
        prediction = prediction.lower()
    if allow_sub_words and len(prediction) > len(ground_truth):
        return min([editdistance.eval(ground_truth, prediction[i:i + len(ground_truth)]) for i in
                    range(len(prediction) - len(ground_truth) + 1)])
    return editdistance.eval(ground_truth, prediction)


def main(config):
    # Read config variables.
    vocabulary = Vocabulary(config.str.vocabulary)
    max_length = config.str.data.max_length
    height = config.str.data.height
    units = config.str.model.units
    grayscale = config.str.data.grayscale
    model_dir = config.str.model.output_dir

    # Create the model.
    model = SceneTextRecognitionModel(
        output_dir=model_dir,
        vocabulary=vocabulary,
        max_length=max_length,
        height=height,
        units=units,
        grayscale=grayscale
    )

    # Create East dataset.
    dataset = EastDataset(
        root='/mnt/dgx/data/CRNN_6/Validation',
        vocabulary=vocabulary,
        height=height,
        max_length=max_length,
        grayscale=grayscale
    )

    model.load()
    loss = 0
    distances = []
    min_distances = []
    for i, (x, y) in enumerate(dataset.batches(1)):
        predictions = model.predict(x, training=False)
        target_word = vocabulary.ohd(y[0], delimiter='')
        predicted_word = vocabulary.ohd(predictions[0], delimiter='')
        distance = compute_edit_distance(target_word, predicted_word, case_sensitive=False, allow_sub_words=False)
        min_distance = compute_edit_distance(target_word, predicted_word, case_sensitive=False, allow_sub_words=True)
        loss += model.compute_loss(y, predictions)
        print(f'{target_word}\t{predicted_word}\t{distance}\t{min_distance}')
        distances.append(distance)
        min_distances.append(min_distance)
        output_dir = f'scratch/attention/{i}_{target_word}'
        os.makedirs(output_dir, exist_ok=True)
        model.visualize_attention(x, output_dir)

    print('Average edit distance: ', sum(distances) / len(distances))
    print('Average minimum edit distance: ', sum(min_distances) / len(distances))


if __name__ == '__main__':
    main(confidence.loadf('scenetextrecognition.yaml'))
